/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Johanny Solano
 */
public class Votacion {
    String idPersona="";
    String idCandidato="";
    String idElección="";
            
    public Votacion(String idPersona,String idCandidato,String idEleccion){
        this.idPersona=idPersona;
        this.idCandidato=idCandidato;
        this.idElección=idEleccion;
    }

    public String getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(String idPersona) {
        this.idPersona = idPersona;
    }

    public String getIdCandidato() {
        return idCandidato;
    }

    public void setIdCandidato(String idCandidato) {
        this.idCandidato = idCandidato;
    }

    public String getIdElección() {
        return idElección;
    }

    public void setIdElección(String idElección) {
        this.idElección = idElección;
    }
    
    
    
}
