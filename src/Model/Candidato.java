/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Johanny Solano
 */
public class Candidato extends Persona {
    
    String partido;
    
    public Candidato(String nombre, String cedula, String partido){
        super(nombre,cedula);
        this.partido = partido;
    }

    public String getPartido() {
        return partido;
    }

    public void setPartido(String partido) {
        this.partido = partido;
    }
    
    
    
    
    
    
}
