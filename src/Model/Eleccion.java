/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.LinkedList;

/**
 *
 * @author Johanny Solano
 */
public class Eleccion {
    String ident = "";
    String periodo="";
    boolean estado = true;
    LinkedList<Candidato> listaCandidatos;
    LinkedList<Votacion> listaVotos;
    
    
    
    public Eleccion(String ident,String periodo,LinkedList<Candidato> listaCandidatos,LinkedList<Votacion> listaVotos){
        this.ident=ident;
        this.periodo=periodo;
        this.estado=true;
        this.listaCandidatos=listaCandidatos;
        this.listaVotos = listaVotos;
    }

    public LinkedList<Votacion> getListaVotos() {
        return listaVotos;
    }

    public void setListaVotos(LinkedList<Votacion> listaVotos) {
        this.listaVotos = listaVotos;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getIdent() {
        return ident;
    }

    public void setIdent(String ident) {
        this.ident = ident;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public LinkedList<Candidato> getListaCandidatos() {
        return listaCandidatos;
    }

    public void setListaCandidatos(LinkedList<Candidato> listaCandidatos) {
        this.listaCandidatos = listaCandidatos;
    }
    
    public void agregarVoto(Votacion vot){
        this.listaVotos.add(vot);
    }
    
    public void agregarCandidato(Candidato cand){
        this.listaCandidatos.add(cand);
    }
    
    
    
    
}
