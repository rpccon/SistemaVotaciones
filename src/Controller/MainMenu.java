/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Persona;
import java.util.LinkedList;
import java.util.Scanner;

import Model.*;

/**
 *
 * @author Johanny Solano
 */
public class MainMenu {
    
    Scanner user_input ;  // definición de método para solicitarle información al cliente
    LinkedList<Persona> listaPersonas;  // definición de arrays globales para manejo de información importante
    LinkedList<Eleccion> listaElecciones;
    LinkedList<Candidato> listaCandidatos;
    public MainMenu(){
        user_input = new Scanner( System.in ); // tomar datos del cliente
        listaPersonas = new LinkedList<Persona>(); // lista general de personas
        listaElecciones = new LinkedList<Eleccion>(); // lista general de elecciones
        listaCandidatos = new LinkedList<Candidato>(); // lista general de candidatos
        
        setPersonas(); // instancia los nuevos objetos persona
        setCandidatos(); // instancia los nuevos candidatos
        setElecciones(); // instancia las elecciones
        setVotosEleccion();// Instancia de votos a la elección incertada
        
    } // constrctor que define los datos iniciales que el proyecto va a necesitar, como elecciones, votos, cantidatos, etc
    public void iniciarSistema(){ // Menú principal de la aplicación
        String menu = "********* Bienvenido al sistema de Votaciones *********\n"+
                "(Escriba el número que indique la acción que desee)\n"+
                "1.Elecciones\n"+
                "2.Personas\n"+
                "3.Candidatos\n";
        pMsj(menu);
        
        String num = user_input.next(); // Entrada de información
        
        if(num.equals("1")){
            menuElecciones();
        }
        else if(num.equals("2")){
            menuPersonas();
        }
        else{
            menuCandidatos();
        }
        
    }// menu principal del sistema
    public void menuElecciones(){
          pMsj("********* Menú Elecciones *********\n"+
                "1.Crear\n"+
                "2.Ver\n"+
                "3.Definir ganador de elecciones\n"+
                "4.Volver");
        String op = user_input.next();
        
        if(op.equals("1")){
            crearNuevaEleccion();
        }
        else if(op.equals("2")){
            this.verElecciones();
            menuElecciones();
        }
        else if(op.equals("3")){
            definirGanadorEleccion();
        }
        else{
            iniciarSistema();
        }        
    }// menú específico para elecciones
    public void definirGanadorEleccion(){// deine cuál de los candidatos con mpas votos es el más elegido
        this.verElecciones();// imprime las elecciones
        
        pMsj("Seleccione el id de la elección");
        String idE = user_input.next();
        Eleccion elec = this.validarExistEleccion(idE);
        if(elec!=null){
            int totalGV = elec.getListaVotos().size();// manejo de total general de votos            
            if(totalGV!=0){
                LinkedList<Resultado> listaResults = administrarEleccion(elec);
                
                Resultado mayor = obtenerResultadoMayor(listaResults);
                
                float numberA = mayor.getVotos();
                float numberB = totalGV;
                float numberRes = (numberA/numberB*100);
                if(numberRes>=40){
                    pMsj("El presidente ganador es : \n"+
                            "Total general de votos: "+String.valueOf(totalGV));
                    
                    Candidato ma = this.validarExistCandidato(mayor.getIdCandidato());
                                     
                    
                    pMsj("ID: "+ mayor.getIdCandidato()+"\n"+
                            "Nombre: "+ma.getNombre()+"\n"+
                            "Total: "+String.valueOf(mayor.getVotos())+"\n");                    
                }
                else{
                    
                    listaResults = eliminarMayorDeLista(listaResults,mayor);
                    Resultado segundo = obtenerResultadoMayor(listaResults);
                    
                    pMsj("El presidente con mayor votos no cuenta con el 40% de los mismos, entonces él y el segundo lugar debe ir a segunda ronda, si desea ver la segunda ronda  debe crear la nueva Elección con estos candidatos");
                    pMsj("************* Resultados: *************\n"+
                            "Total general de votos: "+String.valueOf(totalGV));
                    
                    Candidato ma = this.validarExistCandidato(mayor.getIdCandidato());
                    Candidato seg = this.validarExistCandidato(segundo.getIdCandidato());                    
                    
                    pMsj("ID: "+ mayor.getIdCandidato()+"\n"+
                            "Nombre: "+ma.getNombre()+"\n"+
                            "Total: "+String.valueOf(mayor.getVotos())+"\n");
                    
                    
                    pMsj("ID: "+ segundo.getIdCandidato()+"\n"+
                            "Nombre: "+seg.getNombre()+"\n"+
                            "Total: "+String.valueOf(segundo.getVotos()));                                      
                }
                
                menuElecciones();

            
                
            }
            else{
                pMsj("Error, nadie votó en esta elección");
                errorGetBack(4); 
            }
            
        }
        else{
            pMsj("Error, no existe la elección indicada");
            errorGetBack(4);             
        }
        
    }//Muestra al usuario los arrays de los candidatos más selccionados, en los 2 casos, cuando supera el 40 % y cuando no
    public LinkedList<Resultado> eliminarMayorDeLista(LinkedList<Resultado> lista,Resultado mayor){
        for(int i=0;i<lista.size();i++){
            if(lista.get(i).getIdCandidato().equals(mayor.getIdCandidato())){
                lista.remove(i);
                return lista;
            }
        }
        
        return lista;
    }// una vez que se encuentra el candidato más votado no es necesario tenerlo en la lista
    public Resultado obtenerResultadoMayor(LinkedList<Resultado> lista){
        
        Resultado aux = null;
        for(int i=0;i<lista.size();i++){
            if(i==0){
                aux = lista.get(0);
            }
            else{
                if(lista.get(i).getVotos() > aux.getVotos()){
                   aux =  lista.get(i);
                }
            }
        }
        
        return aux;
    }// retorna el candidato más votado
    public LinkedList<Resultado> administrarEleccion(Eleccion elec){
        LinkedList<Resultado> results = new LinkedList<Resultado>();
        
        for(int i=0;i<elec.getListaVotos().size();i++){
            
            ordenarListaResults(elec.getListaVotos().get(i), results);
        }
        
        return results;
        
        
        
    }//gestiona la elección y genera un array con el total de votos que tuvieron cada uno de los candidatos
    public LinkedList<Resultado> ordenarListaResults(Votacion vota, LinkedList<Resultado> lista){
        
        for(int i = 0; i<lista.size();i++){
            if(lista.get(i).getIdCandidato().equals(vota.getIdCandidato())){
                lista.get(i).setVotos(lista.get(i).getVotos()+1);
                return lista;
            }
        }
        Resultado vota2 = new Resultado(vota.getIdCandidato());
        vota2.setVotos(1);
        lista.add(vota2);
        return lista;
        
    }
    public void crearNuevaEleccion(){
        pMsj("¿ Cuantos candidatos desea nombrar ?");
        String op = user_input.next();
        LinkedList<Candidato> listaC = new LinkedList<Candidato>();
        
        try{
            int num = Integer.valueOf(op);
            if(num>=4){
                this.verPersonas();
               

                for(int i = 0;i<num;i++){
                    pMsj("Indique el id del candidato número "+ String.valueOf(i+1));
                    String id = user_input.next();
                    if(id.equals("")){
                       errorGetBack(4); 
                    }
                    else{
                        for(int h=0;h<listaC.size();h++){
                            if(listaC.get(h).getCedula().equals(id)){
                                pMsj("Se ingresó un id antes ingresaado");
                                errorGetBack(4);
                                
                            }
                        }
                        pMsj("Envía: "+ id);
                        Candidato cand = validarExistCandidato(id);
                        
                        if(cand!=null){
                            listaC.add(cand);
                        }  
                        else{
                            pMsj("No existe el candidato, verifique los datos");
                            errorGetBack(4);
                        }
                    }


                }
                pMsj("¡ Candidatos seleccionados exitosamente !\n Indique el periodo en que se va a dar la elección");
                String periodo = user_input.next();
                if(periodo.equals("")){
                    errorGetBack(4);
                }
                else{
                    String nuevoIdElec = definirIdEleccion();
                    LinkedList<Votacion> vota = new LinkedList<Votacion>();
                    Eleccion nuevaElec = new Eleccion(nuevoIdElec,periodo,listaC,vota);

                    this.listaElecciones.add(nuevaElec);
                    pMsj("Inserción de Elección exitosa, proceda a ver las elecciones en el menú principal para validar resultados");
                    menuElecciones();
                }                
            }
            else{
                pMsj("Error, cada elección mínimo debe tener 4 candidatos");
                menuElecciones();
            }
            
            

            
            
        }
        catch(Exception u){
            errorGetBack(4);
        }
    }//instancia un nuevo elemento cuando se crea una nueva elección
    public String definirIdEleccion(){
        Eleccion ultimo = this.listaElecciones.get(this.listaElecciones.size()-1);
        
        String result = String.valueOf(Integer.valueOf(ultimo.getIdent())+1);
        
        
        return result;
    }// siempre que se crea una nueva elección toma el número de la última elección insertada y le suma uno, para usarse como id de la nueva elección
    public void menuCandidatos(){
         pMsj("********* Menú Candidato *********\n"+
                "1.Registrar candidato a elección\n"+
                "2.Ver candidatos\n"+
                "3.Volver");
        String op = user_input.next();   
        
        if(op.equals("1")){
            pMsj("Estas personas representan los candidatos electos para elecciones\n");
            verElecciones();
            pMsj("(Atención, usted puede asignar a un candidato que forma parte de otra elección a una nueva elección, tambien a alguien que no sea candidato definido)\n Esta otra lista de personas representa todas las personas que pueder jugar un puesto de candidatura, recuerde primero registrarlo en el menu de candidatos: \n");
            this.verPersonas();
            pMsj("Indique el id de de la elección");
            String idE = user_input.next();
            pMsj("Indique el id de del candidato");
            String idC = user_input.next();              
            if(idE.equals("") || idC.equals("")){
                errorGetBack(3);
            }
            else{
                String nPartido = "";
                Candidato prueba = this.validarExistCandidato(idC);
                Persona cand = this.validarExistPersona(idC);
                if(prueba==null && cand!=null){
                    while(nPartido==""){
                        pMsj("Indique el nombre del partido al que va a apoyar el nuevo candidato");
                        nPartido = user_input.next();
                    }
                }
                else{
                    nPartido = prueba.getPartido();
                }
                
               Eleccion elec=validarExistEleccion(idE);                                                         
               if(elec==null || cand == null){
                    errorGetBack(3);
               }  
               else{
                   int res = asignarCandidatoEleccion(idC, idE,nPartido);
                   
                   if(res==0){
                       errorGetBack(3);
                   }
                   else{
                       pMsj("¡ Candidato agregado exitosamente !");
                       menuCandidatos();
                   }
               }
            }
        }
        else if(op.equals("2")){
            verElecciones();
            menuCandidatos();
        }
        else if(op.equals("3")){
            iniciarSistema();
        }        
        else{
            errorGetBack(3);
        }
    }// menú principal para candidatos
    public void menuPersonas(){
        pMsj("********* Menú personas *********\n"+
                "1.Registrar persona\n"+
                "2.Registrar voto\n"+
                "3.Ver personas\n"+
                "4.Volver");
        String op = user_input.next();
        
        if(op.equals("1")){
            registrarPersona();
        }
        else if(op.equals("2")){
            registrarVoto();
        }
        else if(op.equals("3")){
            verPersonas();
            menuPersonas();
        }
        else if(op.equals("4")){
            iniciarSistema();
        }        
        else{
            errorGetBack(2);
        }
    }// menú específico para personas
    public void verPersonas(){
        pMsj("Lista Personas: \n");
        pMsj("Cedula/Nombre:");
        for(int i=0;i<this.listaPersonas.size();i++){
            pMsj("  "+this.listaPersonas.get(i).getCedula()+"/"+this.listaPersonas.get(i).getNombre()+"\n");
               
        }
    }// imprimer el array con todas las personas insertadas
    public Persona validarExistPersona(String id){ // retorna el objeto si existe la persona
        for(int i=0;i<this.listaPersonas.size();i++){
            if(listaPersonas.get(i).getCedula().equals(id)){
                return listaPersonas.get(i);
            }
        }
        return null;
    }//retorna objeto persona si existe
    public Candidato validarExistCandidato(String id){ // retorna el objeto si existe la persona
        for(int i=0;i<this.listaCandidatos.size();i++){
            if(listaCandidatos.get(i).getCedula().equals(id)){
                return listaCandidatos.get(i);
            }
        }
        return null;
    }//retorna objeto candidato si existe
    public Eleccion validarExistEleccion(String id){ // retorna el objeto si existe la persona
        for(int i=0;i<this.listaElecciones.size();i++){
            if(listaElecciones.get(i).getIdent().equals(id)){
                return listaElecciones.get(i);
            }
        }
        return null;
    }//retorna objeto eleccion si existe
    public void verElecciones(){
        for(int i=0;i<this.listaElecciones.size();i++){
            pMsj("********* Elección "+String.valueOf(i+1)+" *********\n"+
                    "Id: "+this.listaElecciones.get(i).getIdent()+" \n"+
                    "Periodo: "+this.listaElecciones.get(i).getPeriodo()+"\n");
            pMsj("          Candidatos:");
            pMsj("              Id/Nombre/Partido");
            for(int e = 0; e<this.listaElecciones.get(i).getListaCandidatos().size();e++){

                pMsj("              "+this.listaElecciones.get(i).getListaCandidatos().get(e).getCedula()+"/"+this.listaElecciones.get(i).getListaCandidatos().get(e).getNombre()+"/"+this.listaElecciones.get(i).getListaCandidatos().get(e).getPartido()+"\n");
            }
        }
    }//imprime los objetos eleccion almacenados en el array
    public void registrarVoto(){
        pMsj("Ingrese la identificación de la persona");
        String id = user_input.next();
        if(id.equals("")){
            errorGetBack(2);
        }
        else{
            
            Persona result = validarExistPersona(id);
            if(result != null){
                verElecciones();
                pMsj("Seleccione id de elección...");
                String idEleccion = user_input.next();
                pMsj("Seleccione id de candidato...");
                String idCand=user_input.next();
                
                
                if(idEleccion.equals("") || idCand.equals("")){
                    errorGetBack(2);
                }
                else{
                    Eleccion elec=validarExistEleccion(idEleccion);
                    Candidato cand = validarExistCandidato(idCand);
                    
                    if(elec==null || cand == null){
                        errorGetBack(2);
                    }
                    else{
                        
                        String res = asignarVotoPersonaCandidatoEleccion(id,idCand,idEleccion);
                        
                        if(res.equals("Error, esta persona ya ejerció su voto en esta elección")){
                            pMsj("Error, esta persona ya ejerció su voto en esta elección");
                            errorGetBack(2);
                        }
                        else if(res.equals("Error, ya las 20 personas permitidas han llegado a su límite")){
                            pMsj("Error, ya las 20 personas permitidas han llegado a su límite");
                            errorGetBack(2);
                        }
                        else{
                            pMsj(res);
                            menuPersonas();
                        }
                        
                    }
                }
                
            }
            else{
                errorGetBack(2);
            }
        }
    }//registra un voto de una persona, a un candidato con una elección
    public String asignarVotoPersonaCandidatoEleccion(String idPer,String idCand, String idElec){
        
        
        for(int i=0;i<this.listaElecciones.size();i++){
            if(this.listaElecciones.get(i).getIdent().equals(idElec)){
                for(int e = 0;e<listaElecciones.get(i).getListaVotos().size();e++){
                    if(listaElecciones.get(i).getListaVotos().get(e).getIdPersona().equals(idPer)){
                        return "Error, esta persona ya ejerció su voto en esta elección";
                    }
                }
                if(listaElecciones.get(i).getListaVotos().size()>20){
                    return "Error, ya las 20 personas permitidas han llegado a su límite";
                }
                else{
                    Votacion vota = new Votacion(idPer,idCand,idElec);
                    listaElecciones.get(i).agregarVoto(vota);
                    return "Se ha generado su voto exitosamente";                    
                }

                
            }
        }
        
        return "";
    }
    public int asignarCandidatoEleccion(String idC, String idE, String aux){
        for(int i=0;i<this.listaElecciones.size();i++){
            if(this.listaElecciones.get(i).getIdent().equals(idE)){
                for(int e=0;e<this.listaElecciones.get(i).getListaCandidatos().size();e++){
                    if(this.listaElecciones.get(i).getListaCandidatos().get(e).getCedula().equals(idC)){
                        return (0);// ya existe el candidato
                    }
                }
                Persona n = this.validarExistPersona(idC);
                
                Candidato nuevoC = new Candidato(n.getNombre(),n.getCedula(),aux);
                this.listaElecciones.get(i).agregarCandidato(nuevoC);
                return 1;
            }
        }
        
        return 0;
    }// asigna un nuevo objeto candidato con una elección    
    public void validarExistCandEnEleccion(){
        
    }
    public void registrarPersona(){
        pMsj("Ingrese la identificación.....");
        String ident = user_input.next();
        if(ident.equals("")){
            errorGetBack(2);
        }
        else{
            
           Persona nueva = validarExistPersona(ident);
           if(nueva==null){
                pMsj("Ingrese el nombre"); 
                String nombre = user_input.next();

                if(nombre.equals("")){
                    errorGetBack(2);
                }
                else{
                    Persona per = new Persona(nombre,ident);
                    this.listaPersonas.add(per);
                    pMsj("¡ Persona agregada satisfactoriamente !");
                    errorGetBack(2);

                }               
           }
           else{
               pMsj("Error, la persona ya existe");
               errorGetBack(2);
           }

        }
        
    }// instancia un nuevo objeto persona en el array global
    public void errorGetBack(int tipo){
        pMsj("Error, datos inconsistentes");
        
        if(tipo==1){
            iniciarSistema();   
        }
        else if(tipo==2){
            menuPersonas();
        }
        else if(tipo==3){
            this.menuCandidatos();
        }
        else{
            this.menuElecciones();
        }
            
    }//siempre que se comete un error y regresa al menú principal
    public void pMsj(String mensaje){// función para imprimir mensaje en consola más eficientemente
        System.out.println(mensaje+"\n");
    }//le muestra un print al usuario para no escribir toda la línea
    public void setPersonas(){
        Persona a = new Persona("Johanny Solano","1");
        Persona b = new Persona("Daniel Castro","2");
        Persona c = new Persona("Derrick Zumbado","3");
        Persona d = new Persona("Marlon Godínez","4");
        Persona e = new Persona("Junal Jaret Jiménez","5");
        Persona f = new Persona("Josser López","6");
        Persona g = new Persona("Andrey Chaverri","7");
        Persona h = new Persona("Emanuel Alfaro","8");    
        Persona i= new Persona("Josué Benavides","9");  
        Persona j = new Persona("Fabricio Miranda","10");  
        Persona k = new Persona("Christopher Liberman","11");  
        Persona l = new Persona("Luis Sirias","12");  
        Persona m = new Persona("Larry Ramirez","13");  
        Persona n = new Persona("Joshua Dixon","14");  
        Persona nn = new Persona("Roberto Salazar","15"); 
        
        this.listaPersonas.add(a);
        this.listaPersonas.add(b);
        this.listaPersonas.add(c);
        this.listaPersonas.add(d);
        this.listaPersonas.add(e);
        this.listaPersonas.add(f);
        this.listaPersonas.add(g);
        this.listaPersonas.add(h);
        this.listaPersonas.add(i);
        this.listaPersonas.add(j);
        this.listaPersonas.add(k);
        this.listaPersonas.add(l);
        this.listaPersonas.add(m);
        this.listaPersonas.add(n);
        this.listaPersonas.add(nn);        
        
        
    }// instancias objetos Persona base al cargar el proyecto
    public void setCandidatos(){
        Candidato a = new Candidato("Johanny Solano","1","Restauración Nacional");
        Candidato b = new Candidato("Daniel Castro","2","Partido Acción Ciudadana");        
        Candidato c = new Candidato("Derrick Zumbado","3","Partido Unidad Social Cristiana");
        Candidato d = new Candidato("Marlon Godínez","4","Partido Liberación Nacional");
        Candidato e = new Candidato("Junal Jaret Jiménez","5","Partido Renovación Costarricense");
        Candidato f = new Candidato("Josser López","6","Partido Frente Amplio"); 
        
        
        this.listaCandidatos.add(a);
        this.listaCandidatos.add(b);
        this.listaCandidatos.add(c);
        this.listaCandidatos.add(d);
        this.listaCandidatos.add(e);
        this.listaCandidatos.add(f);        
    }// instancias objetos Candidato base al cargar el proyecto
    public void setElecciones(){
        LinkedList<Votacion> lista2 = new LinkedList<Votacion>();
        LinkedList<Candidato> lista = new LinkedList<Candidato>();
        lista.add(this.validarExistCandidato("1"));
        lista.add(this.validarExistCandidato("2"));
        lista.add(this.validarExistCandidato("3"));
        lista.add(this.validarExistCandidato("4"));
        Eleccion eleccion = new Eleccion("1","2018-2022",lista,lista2);
        this.listaElecciones.add(eleccion);
        
    }// instancias objetos Eleccion base al cargar el proyecto
    public void setVotosEleccion(){
        String a = asignarVotoPersonaCandidatoEleccion("1","1","1");
        String b = asignarVotoPersonaCandidatoEleccion("2","1","1");
        String c = asignarVotoPersonaCandidatoEleccion("3","1","1");
        String d = asignarVotoPersonaCandidatoEleccion("4","1","1");
        String e = asignarVotoPersonaCandidatoEleccion("5","1","1");
        String f = asignarVotoPersonaCandidatoEleccion("6","1","1");
        String g = asignarVotoPersonaCandidatoEleccion("7","2","1");
        String h = asignarVotoPersonaCandidatoEleccion("8","2","1");
        String i = asignarVotoPersonaCandidatoEleccion("9","2","1");
        String j = asignarVotoPersonaCandidatoEleccion("10","3","1");
        String j11 = asignarVotoPersonaCandidatoEleccion("11","3","1");
        String j12 = asignarVotoPersonaCandidatoEleccion("12","3","1");
        String j13 = asignarVotoPersonaCandidatoEleccion("13","4","1");
        String j14 = asignarVotoPersonaCandidatoEleccion("14","4","1");
        String j15 = asignarVotoPersonaCandidatoEleccion("15","4","1");                      
        
    }// instancias objetos Votación base al cargar el proyecto
}
